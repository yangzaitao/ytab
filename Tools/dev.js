import fs from 'fs';
import path from 'path';
const url = "dist";

const fileUrl = path.join(url, 'index.html');
const code = { encoding: 'utf8' };
fs.readFile(fileUrl, code, (err, data) => {
    if (!err) {
        const data_ = data.replace(/\<div id="app"><\/div>/g, `<iframe style="border:none;" src="http://localhost:3000/"></iframe>`);
        fs.writeFile(fileUrl, data_, code, err => {
            if (!err) {
                console.log('文件写入成功!');
            }
        });
    }
});