import fs, { rmdirSync } from 'fs';
import path from 'path';
const url = 'public/scripts';
const distUrl = 'dist/scripts';
const urls = [url, distUrl];

urls.forEach((dir_url, idx) => {
    fs.readdirSync(dir_url).forEach(file => {
        let url_ = path.join(dir_url, file);
        if (urls[idx] === urls[0]) {
            if (path.extname(url_) === '.js') {
                fs.rmSync(url_);
            }
        } else {
            if (path.extname(url_) === '.ts') {
                fs.rmSync(url_);
            }
        }
    });
});

console.log('清理完成！');