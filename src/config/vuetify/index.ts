import { createVuetify } from 'vuetify'
import { yellow, orange } from "@/config/vuetify/Theme";
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'


const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'mdi',
    },
    theme: {
        defaultTheme: 'orange',
        themes: {
            yellow,
            orange
        },
    },
});

export { vuetify };