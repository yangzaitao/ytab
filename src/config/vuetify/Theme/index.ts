import { ThemeDefinition } from 'vuetify';
const yellow: ThemeDefinition = {
    dark: false,
    colors: {
        'primary-darken-1': '#3700B3',
        'secondary-darken-1': '#018786',
        background: '#fff',
        surface: '#FFf',
        primary: '#6200EE',
        secondary: '#03DAC6',
        error: '#B00020',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    },
}
const orange: ThemeDefinition = {
    dark: false,
    colors: {
        'primary-darken-1': '#3700B3',
        'secondary-darken-1': '#018786',
        background: '#ffffff',
        surface: '#ffffff',
        primary: '#6200EE',
        secondary: '#03DAC6',
        error: '#B00020',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    },
}

export { yellow, orange };