import { createApp } from 'vue';
import { vuetify } from './config/vuetify';
import { router } from '@/config/router';
import App from '@/view/App/index.vue';
import '@/view/App/style.css';

createApp(App)
    .use(router)
    .use(vuetify)
    .mount('#app');
