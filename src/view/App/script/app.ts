import { ref } from "vue";
import { Router } from "vue-router";
enum PageType {
    HOME,
    ABOUT
}

const app = {
    value: ref(0),
    router: ({} as Router),
    tabitem: [
        { icon: 'mdi mdi-semantic-web', txt: 'box', page: PageType.HOME },
        { icon: 'mdi mdi-script-outline', txt: 'about', page: PageType.ABOUT }
    ],
    onDown(page: PageType) {
        switch (page) {
            case PageType.HOME: this.router.push('/'); break;
            case PageType.ABOUT: this.router.push('/about'); break;
        }
    }
}
export { app };