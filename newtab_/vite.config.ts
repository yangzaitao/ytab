import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    port: 3001,
    proxy: {
      '/bing': {
        target: 'https://cn.bing.com',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/bing/, '')
      },
    }
  },
  base: "./",
  build: {
    outDir: "../dist/newtab"
  },
  resolve: {
    alias: {
      "@": resolve(__dirname, "./src")
    }
  }
})
