import { RouteRecordRaw, createRouter, createWebHistory } from "vue-router";


const routes: RouteRecordRaw[] = [
    {
        path: '/',
        component: () => import("@/view/Home/index.vue")
    }
]

const router = createRouter({
    // history: createWebHashHistory(),
    history: createWebHistory(),
    routes,
});

export { router }