import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { aliases, mdi } from 'vuetify/iconsets/mdi'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { Aqua } from './theme/Aqua'
import { Lively } from './theme/Lively'

const themes = { Aqua , Lively };
console.log(themes);
const vuetify = createVuetify({
  components,
  directives,
  theme: {
    defaultTheme: 'Aqua',
    themes: themes
  },
  defaults: {
    VBtn: { elevation: 0 },
    VIcon: { size: 35, color: 'rgb(var(--v-theme-surface))' }
  },
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi,
    },
  }
})

export { vuetify };