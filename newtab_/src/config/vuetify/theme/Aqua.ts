const Aqua = {
    dark: false,
    colors: {
        // background: '#FFD1E3',
        surface: '#A367B1',
        primary: '#5D3587',
        secondary: '#392467',
        error: '#B00020',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    },
}

export { Aqua }