const Lively = {
    dark: false,
    colors: {
        background:"#FF9843",
        surface: '#FFDD95',
        primary: '#86A7FC',
        // 'primary-darken-1': '#3700B3',
        secondary: '#3468C0',
        // 'secondary-darken-1': '#018786',
        error: '#B00020',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    },
}

export { Lively }