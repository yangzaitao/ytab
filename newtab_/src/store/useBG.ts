import { defineStore } from 'pinia';
import { BINGIMGS } from '@/common/api.enum';
import { get, getImg } from '@/common/request';


interface RES {
    title: string;
    data: Data;
    statusCode: number;
    statusMessage: string;
}

interface Data {
    images: Image[];
}

interface Image {
    caption: string;
    title: string;
    description: string;
    copyright: string;
    date: string;
    clickUrl: string;
    imageUrls: ImageUrls;
    descriptionPara2: string;
    descriptionPara3: string;
    isoDate: string;
}

interface ImageUrls {
    landscape: Landscape;
    portrait: Landscape;
}

interface Landscape {
    highDef: string;
    ultraHighDef: string;
    wallpaper: string;
}


export const useBG = defineStore('bg', {
    state: () => ({
        bgs: new Array<Image>()
    }),
    getters: {
        bg(state) {
            return state.bgs[0];
        }
    },
    actions: {
        async getBg() {
            const res = await get<RES>(BINGIMGS);
            const imgs = res.data?.data.images;
            if (imgs !== undefined) {

            }
        }
    }
})