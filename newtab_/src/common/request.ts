type RES<T> = {
    sucess: boolean;
    data?: T;
}
type CallFun = (res: RES<any>) => void;
class Http extends XMLHttpRequest {
    constructor(private url: string, private type: 'GET' | 'POST', callfun: CallFun) {
        super();
        this.open(this.type, this.url, true);
        this.onerror = function () {
            callfun({ sucess: false });
        }
        this.onloadend = function () {
            callfun({ sucess: true, data: this.response });
        }
    }
    setResPonseType(type: XMLHttpRequestResponseType) {
        this.responseType = type;
        return this;
    }
}

function get<T>(url: string) {
    return new Promise<RES<T>>((res) => {
        new Http(url, 'GET', (res_) => res(res_)).setResPonseType('json').send();
    });
}

function getImg(url: string) {
    new Http(url, 'GET', (res) => {
        console.log(res.data);
    }).setResPonseType('blob').send();
}

export { Http, get, getImg };