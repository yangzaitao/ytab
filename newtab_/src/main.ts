import { createApp } from 'vue'
import { router } from './config/router'
import { vuetify } from './config/vuetify'
import { pinia } from './config/pinia'
import './style.css'
import App from './App.vue'

createApp(App).use(vuetify).use(pinia).use(router).mount("#app");
