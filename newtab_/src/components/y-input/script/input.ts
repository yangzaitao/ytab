import { icons } from "@/components/y-drop-menu/script";
import { ref } from "vue";
enum ENGINETYPE {
    BAIDU = "https://www.baidu.com/s?ie=utf-8&wd=",
    BIN = "https://www.bing.com/search?q=",
    GOOGLE = "https://www.google.com/search?q="
}
const iptval = ref('');

function open() {
    if (iptval.value.length > 0) {
        window.open(icons[0].type.concat(iptval.value));
    }
}

export {
    open,
    iptval,
    ENGINETYPE
}