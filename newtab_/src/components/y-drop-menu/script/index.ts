import { reactive } from "vue";
import { ENGINETYPE } from "@/components/y-input/script/input";

interface IconItem {
    icon: string,
    type: ENGINETYPE
}

const icons = reactive<IconItem[]>([
    {
        icon: 'mdi mdi-alpha-b-box',
        type: ENGINETYPE.BAIDU
    },
    {
        icon: 'mdi mdi-alpha-b-box',
        type: ENGINETYPE.BAIDU
    },
    {
        icon: 'mdi mdi-google-plus',
        type: ENGINETYPE.GOOGLE
    },
    {
        icon: 'mdi mdi-microsoft-bing',
        type: ENGINETYPE.BIN
    }]);

function ondown(item: IconItem) {
    icons[0] = item;
}

export { icons, ondown };